/* test that the tr_hypot function is not returning infinity when it
   could return a valid number. the test program exits with an exit
   status of 0 if there were no errors, otherwise with an exit status
   equal to the number of errors found */
#include <stdio.h>
#include <math.h>
  
#include "toy-releng.h"
  
int main(int argc, char *argv[])
{
  int n_errors = 0;
  double x = 2.0;
  double y = 3.0e154;
  double z = tr_hypot(x, y);
  if (!isnormal(z)) {
    printf("ERROR: %g is not normal\n", z);
    ++n_errors;
  }
  /* now try reversing the x and y arguments */
  x = 3.0e154;
  y = 2.0;
  z = tr_hypot(x, y);
  if (!isnormal(z)) {
    printf("ERROR: %g is not normal\n", z);
    ++n_errors;
  }
  if (n_errors > 0) {
    return 1;
  } else {
    return 0;
  }
}
