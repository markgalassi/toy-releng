toy-releng NEWS

* Noteworthy changes in release 1.0.0 (2017-12-14)
** formatting
   Modifications to citation style, twocolumn format adjustments,
   listing parameter adjustments.
** proofreading
   Incorporated detailed proofreading by Laura Fortunato.

* Noteworthy changes in release 0.6.1 (2014-05-02)
** licensing
*** copyright notices
added copyright notices on files longer than 10 lines to prepare them
for submission to savannah.nongnu.org

* Noteworthy changes in release 0.6.0 (2014-04-03)
** Full draft of paper is complete, including abstract.
** Changed build so that paper's PDF file goes into build directory.

* Noteworthy changes in release 0.5.1 (2014-03-10)
** bug fixes
*** fixed tr_hypot to avoid overflow and underflow
** testing
*** set up testing infrastructure for automake
*** added first test program for tr_hypot overflows


* Noteworthy changes in release 0.5.0 (2014-02-27)
** Features
*** Introduced the libtoyreleng library into the build system
*** Top level program is now toy-releng-sample
*** Added significantly to toy-releng-howto.tex
Documented the addition of shared libraries.
Documented the release process for 0.5.0.

* Noteworthy changes in release 0.4.0 (2014-02-24)
** Features
*** Added the ability to build Debian pacakges
We now offer a {\tt debian} subedirectory which allows you to build
debs as described in the paper.
*** Added significantly to toy-releng-howto.tex
Changed document class from article to report and wrote the sections
on debian packaging and using a release branch.

* Noteworthy changes in release 0.3.0 (2014-02-20)
** Features
*** Added the ability to build RPMs
We now offer a {\tt toy-releng.spec.in} file which allows you to build
RPMs with {\tt rpmbuild -ta toy-releng-0.3.0.tar.gz}.
*** Added significantly to toy-releng-howto.tex
Wrote a good part of the "Complete toy-releng" section.

* Noteworthy changes in release 0.2.0 (2014-02-19)
** Features
*** Added interaction of build system with program:
toy-releng now uses PACKAGE and VERSION out of configure.ac
*** Started writing tutorial HOWTO article
There is now a good amount of work in howto-paper/toy-releng-howto.tex

* Noteworthy changes in release 0.1.0 (2014-02-14)
** Features
*** Simple program and build system
Simple program which builds and executes with an autotools build
system.

## Copyright (C) 2014 Los Alamos National Security
