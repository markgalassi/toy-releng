/* Copyright (C) 2014 Los Alamos National Security */
#include <stdio.h>
#include <math.h>
  
#include "toy-releng.h"
#include "config.h"
  
int main(int argc, char *argv[])
{
  printf("welcome to the package %s, version %s\n",
         PACKAGE, VERSION);
  double x = 3.2;
  double x_sq = tr_square(x);
  double x_cu = tr_cube(x);
  printf("square(%f) is %f -- cube(%f) is %f\n", x, x_sq, x, x_cu);
  x = 2.0;
  double y = 3.0;
  printf("hypot(%g, %g) is %g\n", x, y, tr_hypot(x, y));
  return 0;
}
