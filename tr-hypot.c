/* Copyright (C) 2014 Los Alamos National Security */
#include <math.h>
#include "toy-releng.h"

double tr_hypot(double x, double y)
{
  double scale;
  if (x > y) {
    scale = x;
  } else {
    scale = y;
  }
  double a = x/scale;
  double b = y/scale;
  return scale * sqrt(a*a + b*b);
}
