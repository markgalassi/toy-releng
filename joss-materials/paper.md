---
title: 'A self-referential HOWTO on release engineering'
tags:
  - software
  - release engineering
  - howto
  - build systems
  - version control
authors:
 - name: Mark Galassi
   orcid: 0000-0002-3279-2693
   affiliation: "Los Alamos National Laboratory"
affiliations:
 - name: Los Alamos National Laboratory
   index: 1
date: 2017-12-22
bibliography: paper.bib
---

# Summary

toy-releng is a paper on how to do comprehensive and highly principled
release engineering.  This package comes with software which
demonstrates these release engineering approaches.  Much of the
building of this package consists of building the paper and some small
software examples.

-![toy-releng deposited in figshare.](figshare_article.png)

# References
