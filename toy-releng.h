#ifndef _TOY_RELENG_H
#define _TOY_RELENG_H
double tr_square(double x);
double tr_cube(double x);
double tr_hypot(double x, double y);
#endif /* _TOY_RELENG_H */
